package config

import (
	"../errors"
	"../message"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

var Config = make(map[string]int)
//var StringConfig = make(map[string]string)


var filename = "config.txt"
var path = "config/"
var fullPath = path + filename

func handleError(error error, code int, message ...string) (handled bool) {

	if code == errors.LOAD_CONFIG_FAILED {
		defer func() {
			if err := recover(); err != nil {
				//fmt.Println(err)
				if message != nil {
					text := message[0]
					filename := message[1]
					fmt.Printf(text, filename)
				}
				handled = false
			} else {
				handled = true
			}
		}()
	}

	if error != nil {
		panic(error)
	}

	handled = true

	if message != nil && code != errors.LOAD_CONFIG_FAILED {
		msg := message[0]
		fmt.Printf(msg)
	}

	return handled
}

func parseConfig(data []byte) {
	stringData := string(data)
	formattedData := strings.Replace(stringData, "\r\n", "\n", -1)
	splitData := strings.Split(formattedData, "\n")
	for configRow := 0; configRow < len(splitData); configRow++ {
		row := splitData[configRow]
		splitRow := strings.Split(row, " = ")
		option := splitRow[0]
		value, err := strconv.Atoi(splitRow[1])
		handleError(err, errors.IGNORE)
		Config[option] = value
		//StringConfig[option] = splitRow[1]
	}
}

func LoadConfig() error {
	data, err := ioutil.ReadFile(fullPath)
	errorHandled := handleError(err, errors.LOAD_CONFIG_FAILED, message.LOAD_CONFIG_FAILED, filename)
	if !errorHandled {
		return err
	}

	parseConfig(data)
	fmt.Printf(message.LOAD_CONFIG_SUCCESSFUL, filename)

	return nil
}
