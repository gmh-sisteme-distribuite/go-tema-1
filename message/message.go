package message

const (
	//server
	LOAD_CONFIG_SUCCESSFUL = "Config %s has been successfully loaded!\n"
	LAUNCH_SERVER = "Launching server...\n"
	CONNECTION_SUCCESSFUL = "Connection successful!\n"
	CLIENT_CONNECTED = "Client %d connected!\n"
	CLIENT_DISCONNECTED = "Client %d disconnected!\n"
	CLIENT_REQUEST = "Client %d request data: %s\n"
	CLIENT_RESPONSE = "Client %d response data: %d\n"

	//server errors
	LOAD_CONFIG_FAILED = "File %s could not be loaded!\n"
	PARSE_INPUT_TO_INT = "String %s could not be parsed to int!\n"

	//client
	CLIENT_CONNECTION_ESTABLISHED = "Connection to server established successfully!\r\n"
	CLIENT_REQUEST_RECEIVED = "Your request has been successfully received!\r\n"
	SERVER_PROCESSING_INPUT = "The server has started processing your input!\r\n"
	SERVER_REQUEST_DONE = "The server has successfully fulfilled your request!\r\n"
	SERVER_RESPONSE = "The server has sent the response: %s = %s!\r\n"

	//client errors
	MAX_ELEMENTS_EXCEEDED = "Error! Max %s elements allowed!\r\n"
	MIN_ELEMENTS_NOT_MET = "Error! Min %s elements must be input!\r\n"
	MAX_ELEMENT_LENGTH_EXCEEDED = "Error! Elements cannot have more than %s characters!\r\n"
	MIN_ELEMENT_LENGTH_EXCEEDED = "Error! Elements cannot have less than %s characters!\r\n"
	WRONG_ELEMENT_TYPE = "Error! Elements must be integers!\r\n"
	INVALID_SEPARATOR = "Error! A comma followed by space must be used for separating elements!\r\n"

	//app rules
	MAX_ELEMENTS = "Max allowed elements: %s\r\n"
	MIN_ELEMENTS = "Min allowed elements: %s\r\n"
	MAX_ELEMENT_LENGTH = "Max element length: %s\r\n"
	MIN_ELEMENT_LENGTH = "Min element length: %s\r\n"
)

func GetRules() map[string]string {
	rules := make(map[string]string)
	rules["MAX_ELEMENTS"] = "Max allowed elements: %s\r\n"
	rules["MIN_ELEMENTS"] = "Min allowed elements: %s\r\n"
	rules["MAX_ELEMENT_LENGTH"] = "Max element length: %s\r\n"
	rules["MIN_ELEMENT_LENGTH"] = "Min element length: %s\r\n"

	return rules
}