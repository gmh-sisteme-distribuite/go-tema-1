package server

import (
	"../config"
	"../errors"
	"../message"
	"../validator"
	"bufio"
	"bytes"
	"fmt"
	"net"
	"strconv"
	"strings"
	"unicode/utf8"
)

var appRules map[string]string

func handleError(error error, code int, message ...string) (handled bool) {

	if code == errors.CLIENT_DISCONNECTED {
		defer func() {
			if err := recover(); err != nil {
				//fmt.Println(err)
				if message != nil {
					text := message[0]
					client, err := strconv.Atoi(message[1])
					if err != nil {
						client = -1
					}
					fmt.Printf(text, client)
				}
				handled = false
			} else {
				handled = true
			}
		}()
	}

	if error != nil {
		panic(error)
	}

	handled = true

	if message != nil && code != errors.CLIENT_DISCONNECTED {
		msg := message[0]
		fmt.Printf(msg)
	}

	return handled
}

func InitConnection() net.Listener {
	connection, err := net.Listen("tcp", ":8081")
	handleError(err, errors.IGNORE, message.CONNECTION_SUCCESSFUL)

	return connection
}

func Start() {
	//load config
	err := config.LoadConfig()
	if err != nil {
		return
	}

	//load rules
	appRules = message.GetRules()
	fillRules()

	// start server
	fmt.Printf(message.LAUNCH_SERVER)
	connection := InitConnection()

	//map clients by connections so we can access them later
	clientIndex := 0
	clients := make(map[net.Conn]int)

	for {
		//establish connection between client and server
		clientConnection, err := connection.Accept()
		handleError(err, errors.IGNORE)

		//map client for further access
		clients[clientConnection] = clientIndex
		//prepare for next possible client
		clientIndex += 1

		//client side connection successful and everything went right so far
		fmt.Printf(message.CLIENT_CONNECTED, clients[clientConnection])
		_, err = clientConnection.Write([]byte(message.CLIENT_CONNECTION_ESTABLISHED))
		handleError(err, errors.IGNORE)

		go app(clientConnection, clients[clientConnection])
	}
}

//func app(connection net.Listener, clientConnection net.Conn, clients) {
func app(clientConnection net.Conn, client int) {
	reader := bufio.NewReader(clientConnection)

	for {
		//label to return to in case client doesn't respect app config during workflow
		processClientRequest:
			//print app rules for client
			printRules(clientConnection)
			//ask for client input
			_, err := clientConnection.Write([]byte("\r\nArray format: <number><separator = ', '>\r\n"))
			handleError(err, errors.IGNORE)
			_, err = clientConnection.Write([]byte("Input array: "))
			handleError(err, errors.IGNORE)
			//client input
			input, err := reader.ReadString('\n')
			stringClient := strconv.Itoa(client)
			errorHandled := handleError(err, errors.CLIENT_DISCONNECTED, message.CLIENT_DISCONNECTED, stringClient)

			//if client disconnected or something happend with the established connection
			//treat err using recover to silence the panic then do our own logic
			if !errorHandled {
				err := clientConnection.Close()
				handleError(err, errors.IGNORE)
				goto BREAK_COMMUNICATION
			}

			//delete final \n from input
			input = strings.Replace(input, "\r\n", "", -1)

			//input received
			fmt.Printf(message.CLIENT_REQUEST, client, input)
			_, err = clientConnection.Write([]byte(message.CLIENT_REQUEST_RECEIVED))
			handleError(err, errors.IGNORE)

			//server started processing input
			_, err = clientConnection.Write([]byte(message.SERVER_PROCESSING_INPUT))
			handleError(err, errors.IGNORE)

			//validate user input
			validInput, msg := validator.ValidateInput(input)
			if !validInput {
				_, err := clientConnection.Write([]byte(msg))
				handleError(err, errors.IGNORE)
				goto processClientRequest
			}

			//now we can parse user input because it passed our validator
			parsedInput := parseInput(input)

			//lets invert the numbers (strings, we won't parse then to int yet)
			invertedNumbers := invertNumbers(parsedInput)

			//convert strings to ints
			numbers, err, msg := convertStringsToInts(invertedNumbers)
			if err != nil {
				handleError(err, errors.IGNORE, msg)
			}

			//calculate final sum
			sum := sum(numbers)

			//inform the user the request has been fulfilled
			_, err = clientConnection.Write([]byte(message.SERVER_REQUEST_DONE))
			handleError(err, errors.IGNORE)

			//send response to client
			fmt.Printf(message.CLIENT_RESPONSE, client, sum)
			msg = strings.Replace(message.SERVER_RESPONSE, "%s", input, 1)
			msg = strings.Replace(msg, "%s", strconv.FormatInt(sum, 10), 1)
			_, err = clientConnection.Write([]byte(msg))
			handleError(err, errors.IGNORE)
	}
	//label to return when we want to break connection with client
	//or it isn't established anymore
	BREAK_COMMUNICATION:
}

func fillRules() {
	for key, value := range config.Config {
		msg := appRules[key]
		msg = strings.Replace(msg, "%s", strconv.Itoa(value), -1)
		appRules[key] = msg
	}
}

func printRules(clientConnection net.Conn) {
	_, err := clientConnection.Write([]byte("\r\nApp rules\r\n\r\n"))
	handleError(err, errors.IGNORE)
	for _, value := range appRules {
		_, err := clientConnection.Write([]byte(value))
		handleError(err, errors.IGNORE)
	}
}

func sum(numbers []int64) int64 {
	var sum int64 = 0

	for i := range numbers {
		sum += numbers[i]
	}

	return sum
}

func convertStringsToInts(str []string) ([]int64, error, string) {
	numbers := make([]int64, len(str))
	for index := 0; index < len(str); index++ {
		//parse string to int
		number, err := strconv.ParseInt(str[index],10,64)
		if err != nil {
			msg := strings.Replace(message.PARSE_INPUT_TO_INT, "%s", str[index], -1)
			return nil, err, msg
		}
		numbers[index] = number
	}

	return numbers, nil, ""
}

func parseInput(input string) []string {
	splitInput := strings.Split(input, ", ")
	fixedInput := validator.FixSplitInput(splitInput)
	return fixedInput
}

func invertNumbers(numbers []string) []string {
	for index := 0; index < len(numbers); index++ {
		//remove number
		number := numbers[index]
		sign := ""
		if numbers[index][0] == '-' {
			number = numbers[index][1:]
			sign = "-"
		}

		number = reverse(number)
		number = prependStringToString(number, sign)
		numbers[index] = number
	}
	return numbers
}

func prependStringToString(str string, prep string) string {
	//return string(char) + str
	var buffer bytes.Buffer
	buffer.WriteString(prep)
	buffer.WriteString(str)
	return buffer.String()
}

/*
this does not preserve combining unicode characters
but it does preserve any multibyte unicode characters
*/
func reverse(input string) string {
	//init our reversed string with the same bytes length as the original string
	reversed := make([]byte, len(input))
	numberOfBytesWritten := 0

	//here we can substitute the condition with numberOfBytesWritten != original string length
	//because at the end, both strings must have same length
	for len(input) > 0 {
		//extract last rune from original string
		runeValue, runeSize := utf8.DecodeLastRuneInString(input)
		//calculate original string new length after removing last rune
		newInputLength := len(input) - runeSize
		//remove last rune
		input = input[:newInputLength]
		//add extracted rune at the end of the reversed string
		numberOfBytesWritten += utf8.EncodeRune(reversed[numberOfBytesWritten:], runeValue)
	}

	//if we want to reverse the bytes (not runes), just remove the cast to string
	return string(reversed)
}
