package validator

import (
	"../config"
	"../message"
	"fmt"
	"strconv"
	"strings"
	"unicode/utf8"
)

func validateConfig(input []string) (bool, string) {
	//parse stuff for message
	//here we're only returning messages because we don't have access to client connection
	//and tbh, we really shouldn't have

	//validate max elements allowed
	if len(input) > config.Config["MAX_ELEMENTS"] {
		stringMaxElements := strconv.Itoa(config.Config["MAX_ELEMENTS"])
		msg := strings.Replace(message.MAX_ELEMENTS_EXCEEDED, "%s", stringMaxElements, -1)
		return false, msg
	}
	//validate min elements allowed
	if len(input) < config.Config["MIN_ELEMENTS"] {
		stringMinElements := strconv.Itoa(config.Config["MIN_ELEMENTS"])
		msg := strings.Replace(message.MIN_ELEMENTS_NOT_MET, "%s", stringMinElements, -1)
		return false, msg
	}

	//validate element length and type
	for inputIndex := 0; inputIndex < len(input); inputIndex++ {

		//check if element is integer (signed)
		if _, err := strconv.ParseInt(input[inputIndex],10,64); err != nil {
			fmt.Println(err)
			return false, message.WRONG_ELEMENT_TYPE
		}

		//if input has - (minus sign), we must remove it from it's length
		//because we need the number of digits
		inputLength := len(input[inputIndex])

		if input[inputIndex][0] == '-' {
			inputLength--
		}

		//check element max length
		if inputLength > config.Config["MAX_ELEMENT_LENGTH"] {
			stringMaxElementLength := strconv.Itoa(config.Config["MAX_ELEMENT_LENGTH"])
			msg := strings.Replace(message.MAX_ELEMENT_LENGTH_EXCEEDED, "%s", stringMaxElementLength, -1)
			return false, msg
		}

		//check element min length
		if inputLength < config.Config["MIN_ELEMENT_LENGTH"] {
			stringMinElementLength := strconv.Itoa(config.Config["MIN_ELEMENT_LENGTH"])
			msg := strings.Replace(message.MIN_ELEMENT_LENGTH_EXCEEDED, "%s", stringMinElementLength, -1)
			return false, msg
		}
	}

	return true, ""
}

func validateSeparator(input string) (bool, string) {
	//iterate through input and look for our separator combo ", "
	var previousRune rune

	for index, runeWidth := 0, 0; index < len(input); index += runeWidth {
		//decode utf8 bytes value (character) and number of bytes used
		runeValue, width := utf8.DecodeRuneInString(input[index:])

		//get next rune for comparison
		nextIndex := index + width
		nextRune, _ := utf8.DecodeRuneInString(input[nextIndex:])

		//validate
		if (runeValue == ' ' && previousRune != ',') || (runeValue == ',' && nextRune != ' ') {
			return false, message.INVALID_SEPARATOR
		}

		runeWidth = width

		previousRune = runeValue
	}
	return true, ""
}

/*
this is for exceptions like '123, '
because somehow, after split by ', '
split still leaves an empty string in []
*/
func FixSplitInput(input []string) []string {
	response := input

	if input[len(input) - 1] == "" {
		response = response[:len(response) - 1]
	}

	return response
}

func ValidateInput(input string) (bool, string) {
	validSeparator, msg := validateSeparator(input)

	if !validSeparator {
		return validSeparator, msg
	}

	splitInput := strings.Split(input, ", ")
	fixedInput := FixSplitInput(splitInput)
	validInput, msg := validateConfig(fixedInput)

	if !validInput {
		return validInput, msg
	}

	return true, ""
}
